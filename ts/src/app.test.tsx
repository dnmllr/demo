import { shallow } from "enzyme";
import * as React from "react";

import { App } from "./app";

it("renders the heading", () => {
  expect(shallow(<App />).contains(<div>Hello World!!!</div>)).toBeTruthy();
});
