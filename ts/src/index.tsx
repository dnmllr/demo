// tslint:disable no-console
import React from "react";
import { render } from "react-dom";

interface IAppModule {
  App: React.ComponentType;
}

async function loadApp(): Promise<void> {
  const { App } = ((await import("./app")) as unknown) as IAppModule;
  render(<App />, document.getElementById("app"));
}

loadApp().catch((err: Error) => console.error(err));

if (module.hot) {
  module.hot.dispose(() => console.log("module is being disposed of"));
  module.hot.accept(loadApp);
}
