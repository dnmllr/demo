.PHONY: build
build:
	@(cd ./ts && npm run build)
	@(cd ./config/prod && docker-compose build)

.PHONY: up
up:
	@docker-compose -f ./config/dev/docker-compose.yaml up -d

.PHONY: logs
logs:
	@docker-compose -f ./config/dev/docker-compose.yaml logs -f

.PHONY: down
down:
	@docker-compose -f ./config/dev/docker-compose.yaml down -v

.PHONY: test
test:
	@echo "linting go"
	@docker-compose -f ./config/dev/docker-compose.yaml exec -T api /go/bin/golangci-lint run ./...
	@echo "testing backend"
	@docker-compose -f ./config/dev/docker-compose.yaml exec -T api go test ./...
	@echo "linting ts"
	@docker-compose -f ./config/dev/docker-compose.yaml exec -T frontend npm run lint
	@echo "testing frontend"
	@docker-compose -f ./config/dev/docker-compose.yaml exec -T frontend npm test

clean: down
	@rm -f api
	@docker system prune -f
	@docker volume prune -f