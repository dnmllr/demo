package http

import (
	"fmt"
	"log"
	"net/http"
)

func Default(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
	if err != nil {
		log.Printf("Unable to write to response writer with error: %v", err.Error())
	}
}
