package http

import (
	"fmt"
	"net/http/httptest"
	"testing"
)

func testDefaultWithPath(t *testing.T, path string) {
	resp := httptest.NewRecorder()
	req := httptest.NewRequest("GET", path, nil)
	Default(resp, req)
	expect := fmt.Sprintf("Hello, you've requested: %s\n", path)
	if resp.Body.String() != expect {
		t.Fatalf("Expected request at path %v to return %v", path, expect)
	}

}

func TestDefault(t *testing.T) {
	testDefaultWithPath(t, "/")
	testDefaultWithPath(t, "/hello")
}

