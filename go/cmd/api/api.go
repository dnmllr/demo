package main

import (
	"fmt"
	"log"
	"net/http"

	h "gitlab.com/dnmllr/demo/lib/http"
)

const PortNo = "8000"

func main() {
	log.Print("Starting...")
	http.HandleFunc("/", h.Default)

	log.Printf("Listing on port: %v...", PortNo)

	if err := http.ListenAndServe(fmt.Sprintf(":%v", PortNo), nil); err != nil {
		panic(fmt.Sprintf("unable to start server with error: %v", err.Error()))
	}
}
